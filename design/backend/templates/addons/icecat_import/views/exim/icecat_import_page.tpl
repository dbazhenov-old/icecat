{capture name="mainbox"}
<div id="content_icecat">
	{assign var="use_ajax" value="Y"}
	{include file="common/subheader.tpl" title=__("import_options")}
	<form action="{""|fn_url}" method="post" name="icecat_import_form" enctype="multipart/form-data" class="form-horizontal form-edit"> <!--cm-ajax cm-comet-->
		<input type="hidden" name="section" value="icecat_import" />
		<div class="control-group">
			<label class="control-label" for="update_icecat_type">{__("update_icecat_type")}:</label>
			<div class="controls">
                <select name="import_options[update_icecat_type]" id="update_icecat_type" style="width: 345px;">
                    <option value="EAN" checked="checked">{__("update_icecat_type_ean")}</option>
                    <option value="MAN">{__("update_icecat_type_man_id")}</option>
                </select>
			</div>
		</div>

		{include file="common/subheader.tpl" title=__("icecat_import_pane")}

        <div class="control-group">
            <label class="control-label" for="update_name">{__("name")}:</label>
            <div class="controls">
                <input type="hidden" name="import_options[update_name]" value="N" />
                <input id="update_name" class="checkbox" type="checkbox" name="import_options[update_name]" value="Y"/>
            </div>
        </div>

		<div class="control-group">
			<label class="control-label" for="update_short_description">{__("short_description")}:</label>
			<div class="controls">
                <input type="hidden" name="import_options[update_short_description]" value="N" />
                <input id="update_short_description" class="checkbox" type="checkbox" name="import_options[update_short_description]" value="Y"/>
            </div>
		</div>

		<div class="control-group">
			<label class="control-label" for="update_long_description">{__("full_description")}:</label>
			<div class="controls">
                <input type="hidden" name="import_options[update_long_description]" value="N" />
                <input id="update_long_description" class="checkbox" type="checkbox" name="import_options[update_long_description]" value="Y"/>
            </div>
		</div>

		<div class="control-group">
			<label class="control-label" for="update_images">{__("images")}:</label>
			<div class="controls">
                <input type="hidden" name="import_options[update_images]" value="N" />
                <input id="update_images" class="checkbox" type="checkbox" name="import_options[update_images]" value="Y"/>
            </div>
		</div>

		<div class="control-group">
			<label class="control-label" for="update_features">{__("features")}:</label>
			<div class="controls">
                <input type="hidden" name="import_options[update_features]" value="N" />
                <input id="update_features" class="checkbox" type="checkbox" name="import_options[update_features]" value="Y"/>
           </div>
		</div>

		<div class="control-group">
			<label class="control-label" for="update_pdf_files">{__("pdf_files")}:</label>
			<div class="controls">
                <input type="hidden" name="import_options[update_pdf_files]" value="N" />
                <input id="update_pdf_files" class="checkbox" type="checkbox" name="import_options[update_pdf_files]" value="Y"/>
            </div>
		</div>

		<div class="control-group">
			<label class="control-label" for="update_multimedia">{__("multimedia")}:</label>
			<div class="controls">
                <input type="hidden" name="import_options[update_multimedia]" value="N" />
                <input id="update_multimedia" class="checkbox" type="checkbox" name="import_options[update_multimedia]" value="Y"/>
            </div>
		</div>

		<!--div class="control-group">
			<label class="control-label" for="update_accessories">{__("accessories")}:</label>
			<div class="controls">
                <input type="hidden" name="import_options[update_accessories]" value="N" />
                <input id="update_accessories" class="checkbox" type="checkbox" name="import_options[update_accessories]" value="Y"/>
            </div>
		</div-->

		{include file="common/subheader.tpl" title=__("icecat_import_products")}

		<div id="content_manage_products" style="padding: 5px 25px 25px 25px;">

			{include file="common/pagination.tpl" save_current_page=true save_current_url=true div_id=$smarty.request.content_id}

            {assign var="c_url" value=$config.current_url|fn_query_remove:"sort_by":"sort_order"}
            {assign var="rev" value=$smarty.request.content_id|default:"pagination_contents"}
            {assign var="c_icon" value="<i class=\"exicon-`$search.sort_order_rev`\"></i>"}
            {assign var="c_dummy" value="<i class=\"exicon-dummy\"></i>"}
            {if $products}
			<table width="100%"  class="table table-middle">
			<thead>
			<tr>
				<th class="left">
                    <input type="checkbox" name="check_all" value="Y" title="{__("check_uncheck_all")}" class="checkbox cm-check-items" />
                </th>
				<th width="5%" class="nowrap"><span>{__("image")}</span></th>
				<th width="25%" class="nowrap">
                    <a class="cm_ajax" href="{"`$c_url`&sort_by=product&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("name")}{if $search.sort_by == "product"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a>
                 </th>
				<th width="10%" class="nowrap">
                    <a class="cm_ajax" href="{"`$c_url`&sort_by=code&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("product_code")}{if $search.sort_by == "code"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a>
                </th>
				<th width="15%" class="nowrap">{__("ean_upc")}</th>
				<th width="10%" class="nowrap">{__("icecat_vendor_name")}</th>
				<th width="10%" class="nowrap">{__("icecat_last_update")}</th>
				<th width="10%" class="nowrap">{__("status")}</th>
				<th width="10%" class="nowrap">&nbsp;</th>
			</tr>
			</thead>

			{foreach from=$products item=product}
			<tr class="{cycle values="table-row,"} {$hide_inputs_if_shared_product}">
				<td class="left">
					<input type="checkbox" name="product_ids[]" value="{$product.product_id}" class="checkbox cm-item" />
				</td>
				<td class="product-image-table">
					{include file="common/image.tpl" image=$product.main_pair.icon|default:$product.main_pair.detailed image_id=$product.main_pair.image_id image_width=30 object_type=$object_type href="products.update?product_id=`$product.product_id`"|fn_url}
				</td>
				<td>
					<div>
						<a href="{"products.update?product_id=`$product.product_id`"|fn_url}" class="strong{if $product.status == "N"} manage-root-item-disabled{/if}">{$product.product|unescape} {include file="views/companies/components/company_name.tpl" company_name=$product.company_name company_id=$product.company_id}</a></div>
					</div>
				</td>
				<td>
					<div>{$product.product_code}</div>
				</td>
				<td>
					<div>{$product.icecat_manufacturer_code}</div>
				</td>
				<td>
					<div>{$product.icecat_vendor_code}</div>
				</td>
				<td class="nowrap">
					<div>
                    {$product.icecat_timestamp|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}
					</div>
				</td>
				<td class="nowrap">
					<div id="check_EAN_{$product.product_id}">
                        <div
                                    {if $product.icecat_status_ean == "Y"}
                                        style="color: green;"
                                    {elseif $product.icecat_status_ean == "N"}
                                        style="color: red;"
                                    {/if}"
                                    >
                            {if $product.icecat_status_ean == "Y"}
                                EAN available
                            {elseif $product.icecat_status_ean == "N"}
                                EAN not available{else}ean not check
                            {/if}
                        </div>
                    <!--check_EAN_{$product.product_id}--></div>
					<div id="check_MAN_{$product.product_id}">
                        <div
                                    {if $product.icecat_status_man == "Y"}
                                        style="color: green;"
                                    {elseif $product.icecat_status_man == "N"}
                                        style="color: red;"
                                    {/if}"
                                    >
                            {if $product.icecat_status_man == "Y"}
                                MAN available
                            {elseif $product.icecat_status_man == "N"}
                                MAN not available
                            {else}
                                man not check
                            {/if}
                        </div>
                    <!--check_MAN_{$product.product_id}--></div>
				</td>
				<td class="nowrap">
					<div>
                        <a {if $use_ajax}
                                class="cm-ajax"
                           {/if} 
                           href="{"exim.check_product?product_id=`$product.product_id`&type=EAN&value=`$product.icecat_manufacturer_code`"|fn_url}" 
                           data-ca-target-id="check_EAN_{$product.product_id}"
                           >
                                {__("check_ean")}
                       </a>
                   </div>
					<div>
                        <a {if $use_ajax}
                                class="cm-ajax"
                           {/if} 
                           href="{"exim.check_product?product_id=`$product.product_id`&type=MAN&value=`$product.product_code`&vendor=`$product.icecat_vendor_code`"|fn_url}" 
                           data-ca-target-id="check_MAN_{$product.product_id}"
                           >
                                {__("check_man")}
                       </a>
                   </div>
				</td>
			</tr>
			{/foreach}
			</table>
            {else}
                <p class="no-items">{__("no_data")}</p>
            {/if}
			{include file="common/pagination.tpl" div_id=$smarty.request.content_id}
		<!--content_manage_products--></div>



		{capture name="buttons"}
            {include file="buttons/button.tpl" 
                but_text=__("update_products") 
                but_name="dispatch[exim.icacat_import_products]" 
                but_role="submit-link"
                but_target_form="icecat_import_form"
                }
            {include file="buttons/button.tpl" 
                but_text=__("update_all_products") 
                but_name="dispatch[exim.icacat_import_all_products]" 
                but_role="submit-link" 
                but_target_form="icecat_import_form"
                }
		{/capture}
	</form>
<!--content_icecat--></div>

{/capture}
{include file="common/mainbox.tpl" title=__("icecat_import_page") content=$smarty.capture.mainbox buttons=$smarty.capture.buttons}