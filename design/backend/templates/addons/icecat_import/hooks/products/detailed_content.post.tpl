{include file="common/subheader.tpl" title=__("icecat_settings") target="#icecat_import"}
<div id="icecat_import" class="collapsed in">
<fieldset>
    <div class="control-group">
        <label class="control-label" for="product_icecat_product">{__("icecat_product")}:</label>
        <div class="controls">
            <input type="hidden" name="product_data[icecat_product]" value="N" />
            <input type="checkbox" name="product_data[icecat_product]" id="product_icecat_product" value="Y" {if $product_data.icecat_product == "Y"}checked="checked"{/if} class="checkbox" />
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="icecat_manufacturer_code">{__("ean_upc")}:</label>
        <div class="controls">
            <input type="text" name="product_data[icecat_manufacturer_code]" size="10" id="icecat_manufacturer_code" value="{$product_data.icecat_manufacturer_code}" class="input-long-short" />
        </div>
    </div>

    <div class="control-group">
    <label class="control-label" for="icecat_vendor_code">{__("icecat_vendor_code")}:</label>
    <div class="controls">
            <input type="text" name="product_data[icecat_vendor_code]" size="10" id="icecat_vendor_code" value="{$product_data.icecat_vendor_code}" class="input-long-short" />
        </div>
    </div>
</fieldset>
</div>