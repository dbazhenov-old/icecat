{if $field=='icecat_product'}
    <input type="hidden" name="products_data[{$product.product_id}][icecat_product]" value="N" />
    <input type="checkbox" 
        name="products_data[{$product.product_id}][icecat_product]" 
        id="icecat_product_{$product.product_id}" 
        {if $product.icecat_product == 'Y'}
            checked="checked"
        {/if}
            value="Y"
    />
{else}
    <input type="text" value="{$product.$field}" class="input-medium" name="products_data[{$product.product_id}][{$field}]" />
{/if}