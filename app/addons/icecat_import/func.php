<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if ( !defined('AREA') ) { die('Access denied'); }

function fn_icecat_import_get_products($params, $fields, $sortings, &$condition, $join, $sorting, $group_by, $lang_code) {

	if (!empty($params['icecat_product']) && $params['icecat_product']) {
		$condition .= db_quote(' AND products.icecat_product = "Y"');
	}
}

function fn_icecat_product_avail($product_id, $type, $status) {
	if (!empty($product_id)) {
		if ($type == "EAN") {
			db_query("UPDATE ?:products SET icecat_status_ean = ?s WHERE product_id = ?i", $status, $product_id);
		} elseif ($type == "MAN") {
			db_query("UPDATE ?:products SET icecat_status_man = ?s WHERE product_id = ?i", $status, $product_id);
		}
		return true;
	}
	return false;
}

function fn_icecat_import_update_product_pre(&$product_data, $product_id, $lang_code) {
	$old_product_data = fn_get_product_data($product_id, $auth, DESCR_SL, '', true, true, true, true);
	if ($old_product_data['icecat_manufacturer_code'] != $product_data['icecat_manufacturer_code']) {
		$product_data['icecat_status_ean'] = "C";
	}
	if ($old_product_data['product_code'] != $product_data['product_code'] || $old_product_data['icecat_vendor_code'] != $product_data['icecat_vendor_code']) {
		$product_data['icecat_status_man'] = "C";
	}
}

function fn_update_ice_products($options, $products_ids = array()) {
    $ajax = Registry::get('ajax');

    if (!list($catalog,$catalog_ean,$catalog_man) = get_ICE_catalog()) {
        return false;
    }

    if (!list($suppliers) = get_ICE_suppliers()) {
        return false;
    }

    $params['icecat_product'] = true;
    if ($products_ids) $params['pid'] = $products_ids;

    list($products, $search, $product_count) = fn_get_products($params, 0, DESCR_SL);
    unset($search);

    //fn_print_r($products_ids);

    if (!empty($products)) {
        //fn_set_progress('total', $product_count);
        $step = 1;
        foreach ($products as $product) {
			$force_update = false;
            if (empty($product['tax_ids'])) unset($product['tax_ids']);
            //fn_set_progress('echo', "update product #{$step} of the {$product_count}", true);
            $ice_product_id = 0;

            if ($options['update_icecat_type'] == "EAN" && ($product['icecat_status_ean'] == "Y" || empty($products_ids))) {
				$force_update = "EAN";
                $ice_product_id = (@isset($catalog_ean[$product['icecat_manufacturer_code']])) ? $catalog_ean[$product['icecat_manufacturer_code']] : $ice_product_id;
            } else if ($options['update_icecat_type'] == "MAN" && ($product['icecat_status_man'] == "Y" || empty($products_ids))) {
				$force_update = "MAN";
                $ice_product_id = (@isset($catalog_man[$suppliers[$product['icecat_vendor_code']]][$product['product_code']])) ? $catalog_man[$suppliers[$product['icecat_vendor_code']]][$product['product_code']] : $ice_product_id;
            }

            //fn_print_die($product['product_code'], $catalog_ean, $ice_product_id);

            if (!empty($ice_product_id) || $force_update) {
                update_ICE_product_data($catalog[$ice_product_id],$options, $product, $force_update);
                if (empty($products_ids)) {
                    fn_icecat_product_avail($product['product_id'], $options['update_icecat_type'], 'Y');
                }
            } else {
                if (empty($products_ids)) {
                    fn_icecat_product_avail($product['product_id'], $options['update_icecat_type'], "N");
                }
            }
        }
    }

}

function update_ICE_product_data($url, $options, $product_data, $force_update = false) {
    // Username and password for usage with ICEcat
    $username = Registry::get('addons.icecat_import.username');
    $password = Registry::get('addons.icecat_import.password');
    $account_type = Registry::get('addons.icecat_import.account_type');

    // Get the product specifications in XML format
    /*$context = stream_context_create(array(
        'http' => array(
            'header'  => "Authorization: Basic " . base64_encode($username.":".$password)
        )
    ));*/

    if (empty($url) && !$force_update) return false;

    if (!empty($url)) {
		$data = @file_get_contents("http://" . $username . ":" . $password . "@" . "data.Icecat.biz/" . $url, false, $context);
	} else {
		if ($force_update == "EAN") {
			$data = file_get_contents('http://' . $username . ":" . $password . "@" . 'data.icecat.biz/xml_s3/xml_server3.cgi?ean_upc='.$product_data['icecat_manufacturer_code'].';lang=EN;output=productxml', false, $context);
		} else if ($force_update == "MAN") {
			$data = file_get_contents('http://' . $username . ":" . $password . "@" . 'data.icecat.biz/xml_s3/xml_server3.cgi?prod_id='.$product_data['product_code'].';vendor='.$product_data['icecat_vendor_code'].';lang=EN;output=productxml', false, $context);
		}
	}

	//fn_print_die('http://' . $username . ":" . $password . "@" . 'data.icecat.biz/xml_s3/xml_server3.cgi?prod_id='.$product_data['icecat_vendor_code'].';vendor='.$product_data['product_code'].';lang=EN;output=productxml',$data);

    $xml = new SimpleXMLElement($data);
    unset($data);

    if ($xml->Product['Code'] != "1") {
        return array ("code" => false, "content" => (string) $xml->Product['ErrorMessage']);
    }

    $attach_url = array();

    if ($options['update_name'] == "Y") {
        $product_data['product'] = (string) $xml->Product['Title'];
    }

    if ($options['update_short_description'] == "Y") {
        $product_data['short_description'] = (string) $xml->Product->ProductDescription['ShortDesc'];
        $product_data['short_description'] = str_replace('\n', '<br />', $product_data['short_description']);
    }

    if ($options['update_long_description'] == "Y") {
        $product_data['full_description'] = str_replace('\n', '<br />', (string) $xml->Product->ProductDescription['LongDesc']);
    }

    if ($options['update_features'] == "Y") {

        $ice_CategoryFeatureGroup = $xml->xpath("//CategoryFeatureGroup");
        $ice_ProductFeature = $xml->xpath("//ProductFeature");

        //Draw product specifications table if any specs available for the product
        if($ice_ProductFeature != null) {

            $groups = array();
            foreach($ice_CategoryFeatureGroup as $cat_group) {
                $catId = (int) $cat_group["ID"];
                $title = (string) $cat_group->FeatureGroup->Name["Value"];
                $groups[$catId]['name'] = $title;
            }

            $product_features = array();
            foreach($ice_ProductFeature as $ice_feature) {
                $title = (string) $ice_feature->Feature->Name["Value"];
                if ($title != "Source data-sheet") {
                    $group_id = (int) $ice_feature["CategoryFeatureGroup_ID"];
                    $feature_id = (int) $ice_feature->Feature["ID"];
                    $value = str_replace('\n', '<br />', (string) $ice_feature["Presentation_Value"]);
                    $product_features[$group_id][$feature_id]["name"] = $title;
                    $product_features[$group_id][$feature_id]["value"] = $value;
                }
            }
        }

        $product_data["product_features"] = fn_update_features_from_ice($product_data, $groups, $product_features, "EN");
    }

    if ($options['update_images'] == "Y") {
        $url_main = (string) $xml->Product['HighPic'];

        if (!empty($url_main)) {
            fn_delete_image_pairs($product_data['product_id'], 'product');

            fn_prepare_image_fake_main_data();
            fn_prepare_image_fake_main_request($url_main);

            // Update main images pair
            fn_attach_image_pairs('product_main', 'product', $product_data['product_id'], "EN");
        }

        $ice_ProductGalery = $xml->xpath("//ProductPicture");
        $add_images_id = 0;

        foreach($ice_ProductGalery as $ice_image) {
            $url_addition = (string) $ice_image['Pic'];
            if (!empty($url_addition)) {
                fn_prepare_image_fake_add_additional_data($add_images_id);
                fn_prepare_image_fake_add_additional_request($url_addition, $add_images_id);
                $add_images_id++;
            }
        }

        if ($add_images_id > 0) {
            // Update additional images
            fn_attach_image_pairs('product_add_additional', 'product', $product_data['product_id'], "EN");
        }
    }

    $attach_url = array();

    //FIXME
    if ($options['update_pdf_files'] == "Y" || $options['update_multimedia'] == "Y") {
        $attachments = fn_get_attachments('product', $product_data['product_id'], 'M', "EN");
        foreach ($attachments as $attach_data) {
            fn_delete_attachments(array($attach_data['attachment_id']), 'product', $product_data['product_id']);
        }
    }

    if ($options['update_pdf_files'] == "Y") {

        $pdf_manual = (string) $xml->Product->ProductDescription['ManualPDFURL'];
        if (!empty($pdf_manual)) $attach_url[] = $pdf_manual;
        $pdf_descr = (string) $xml->Product->ProductDescription['PDFURL'];
        if (!empty($pdf_descr)) $attach_url[] = $pdf_descr;
    }

    if ($options['update_multimedia'] == "Y") {
        $ice_MultimediaObject = $xml->xpath("//MultimediaObject");

        foreach($ice_MultimediaObject as $ice_m_object) {
            $new_obj = (string) $ice_m_object['URL'];
            if (!empty($new_obj)) $attach_url[] = $new_obj;
        }
    }

    if (!empty($attach_url)) {
        foreach ($attach_url as $file_url) {
            $file_name = basename($file_url);
            fn_prepare_attachment_fake_request($file_name, $file_url);
            fn_update_attachments($_REQUEST['attachment_data'], 0, 'product',  $product_data['product_id'], 'M', null, DESCR_SL);
        }
    }

    $product_data["icecat_timestamp"] = time();

    fn_update_product($product_data, $product_data['product_id']);

}

function fn_prepare_attachment_fake_request($name, $url) {
    $_REQUEST['attachment_data'] = array (
        'description' => $name,
        'position' => "",
        'usergroup_ids' => 0
    );
    $_REQUEST['file_attachment_files'][0] = $url;
    $_REQUEST['type_attachment_files'][0] = 'url';
}

function fn_prepare_image_fake_main_data() {
    $_REQUEST['product_main_image_data'] = array(
        "0" => array (
            'pair_id' => 0,
            'type' => 'M',
            'object_id' => 0,
            'image_alt' => '',
            'detailed_alt' =>'',
        )
    );

	/*
    $_REQUEST['product_main_image_data']["0"] => array (
        'pair_id' => 0, 'type' => 'M', 'object_id' => 0, 'image_alt' => '', 'detailed_alt' =>''
    );
	*/
}

function fn_prepare_image_fake_main_request($url) {
    $_REQUEST['file_product_main_image_detailed'][0] = $url;
    $_REQUEST['type_product_main_image_detailed'][0] = 'url';
}



function fn_prepare_image_fake_add_additional_data($id) {
    $_REQUEST['product_add_additional_image_data'][$id] = array(
        'pair_id' => '', 'type' => 'A',    'object_id' => 0, 'image_alt' => '', 'detailed_alt' => ''
    );
    $_REQUEST['file_product_add_additional_image_icon'][$id] = 'product_add_additional';
    $_REQUEST['type_product_add_additional_image_icon'][$id] = 'local';
}

function fn_prepare_image_fake_add_additional_request($url, $id) {

    $_REQUEST['file_product_add_additional_image_detailed'][$id] = $url;
    $_REQUEST['type_product_add_additional_image_detailed'][$id] = 'url';
}

function fn_update_features_from_ice($product_data, $ice_groups, $ice_features, $lang_code = CART_LANGUAGE) {

    $product_features = array();

    foreach ($ice_groups as $ice_f_g_id => $ice_group) {
        if (isset ($ice_features[$ice_f_g_id])) {
            $f_g_id = fn_get_group_id_by_name($ice_group['name'], $lang_code);

            if (intval($f_g_id)) {
                $group_id = $ice_groups[$ice_f_g_id]["feature_id"] = $f_g_id;
            } else {
                $group_id = $ice_groups[$ice_f_g_id]["feature_id"] = fn_create_new_feature_group($ice_group['name'],$lang_code);
            }


            foreach ($ice_features[$ice_f_g_id] as $ice_f_id => $ice_feature) {
                $f_id = fn_get_feature_id_by_name($group_id, $ice_feature["name"], $lang_code);

                if (intval($f_id)) {
                    $feature_id = $ice_features[$ice_f_g_id][$ice_f_id]["feature_id"] = $f_id;
                } else {
                    $feature_id = $ice_features[$ice_f_g_id][$ice_f_id]["feature_id"] = fn_create_new_feature($ice_feature['name'], $group_id ,$lang_code);
                }

                $v_id = fn_get_variant_id_by_name($f_id, $ice_feature["value"], $lang_code);

                if (intval($v_id)) {
                    $variant_id = $ice_features[$ice_f_g_id][$ice_f_id]["variant_id"] = $v_id;
                } else {
                    $variant_id = $ice_features[$ice_f_g_id][$ice_f_id]["variant_id"] = fn_add_feature_variant($feature_id, array("variant" => $ice_feature["value"]));
                }

                $product_features[$feature_id] = $variant_id;
            }
        }
    }

    return $product_features;
}

function fn_create_new_feature_group($name,$lang_code) {

    $company_id = Registry::get('runtime.company_id');
    $feature_data = array (
        'description' => $name,
        'company_id' => $company_id,
        'feature_code' => '',
        'position' => '',
        'full_description' => '',
        'feature_type' => 'G',
        'display_on_product' => 'Y',
        'display_on_catalog' => 'Y',
        'display_on_header' => 'N',
        'original_var_ids' => '',
        'variants' => array (
            '1' => array
                (
                    'position' => '',
                    'variant' => '',
                    'description' => '',
                    'page_title' => '',
                    'url' => '',
                    'meta_description' => '',
                    'meta_keywords' => ''
                )
        ),
        'categories_path' => ''
    );
    Registry::set('sharing_owner.product_features', $company_id);
    return fn_update_product_feature($feature_data, NEW_FEATURE_GROUP_ID, $lang_code);
}

function fn_create_new_feature($name, $group_id, $lang_code) {
    $company_id = Registry::get('runtime.company_id');
    $feature_data = array (
        "company_id" => $company_id,
        "description" => $name,
        "position" => "",
        "full_description" => "",
        "feature_type" => "S",
        "parent_id" => $group_id,
        //[mviktorov]
        //"display_on_product" => 0,
        //"display_on_catalog" => 0,
        "display_on_product" => "Y",
        "display_on_catalog" => "Y",
        //[/mviktorov]
        "categories_path" => ""
    );
    return fn_update_product_feature($feature_data, 0 ,$lang_code);
}

function fn_get_group_id_by_name($name, $lang_code) {
    //[mviktorov]
    //$group_id = db_get_field('SELECT feature_id FROM ?:product_features_descriptions WHERE description LIKE ?s AND lang_code = ?s', $name, $lang_code);
    $group_id = db_get_field('SELECT ?:product_features_descriptions.feature_id '
                            .'FROM ?:product_features_descriptions '
                            .'LEFT JOIN ?:product_features '
                            .'ON ?:product_features.feature_id = ?:product_features_descriptions.feature_id '
                            .'WHERE ?:product_features_descriptions.description LIKE ?s '
                            .'AND ?:product_features_descriptions.lang_code = ?s '
                            .'AND ?:product_features.feature_type = "G"', 
                            $name, $lang_code
                            );
    //[/mviktorov]
    if (!empty($group_id))
        return $group_id;
    else
        return false;
}

function fn_get_feature_id_by_name($parent_id, $name, $lang_code) {
    $feature_id = db_get_field('SELECT pfd.feature_id FROM ?:product_features_descriptions as pfd LEFT JOIN ?:product_features ON ?:product_features.feature_id = pfd.feature_id WHERE ?:product_features.parent_id = ?i AND pfd.description = ?s AND pfd.lang_code = ?s', $parent_id, $name, $lang_code);
    if (!empty($feature_id))
        return $feature_id;
    else
        return false;
}

function fn_get_variant_id_by_name($feature_id, $name, $lang_code) {
    $variant_id = db_get_field("SELECT pfvd.variant_id FROM ?:product_feature_variant_descriptions as pfvd LEFT JOIN ?:product_feature_variants ON ?:product_feature_variants.variant_id = pfvd.variant_id WHERE ?:product_feature_variants.feature_id = ?i AND pfvd.variant = ?s AND pfvd.lang_code = ?s", $feature_id, $name, $lang_code);
    if (!empty($variant_id))
        return $variant_id;
    else
        return false;
}

function get_ICE_catalog($lang = "EN") {
	// Username and password for usage with ICEcat
	$username = Registry::get('addons.icecat_import.username');
	$password = Registry::get('addons.icecat_import.password');
	$account_type = Registry::get('addons.icecat_import.account_type');

	if ($account_type == "free") {
		$url_request = "http://" . $username . ":" . $password . "@" . "data.Icecat.biz/export/freexml/" . $lang . "/";
	} else {
		$url_request = "http://" . $username . ":" . $password . "@" . "data.Icecat.biz/export/level4/" . $lang . "/";
	}

	// Get the product specifications in XML format
	/*$context = stream_context_create(array(
		'http' => array(
			'header'  => "Authorization: Basic " . base64_encode($username.":".$password)
		)
	));*/

    //fn_set_progress('echo', fn_get_lang_var('request_catalog'), false);

	$data = file_get_contents($url_request, false, $context);

    if (!empty($data)) {

        //fn_set_progress('echo', fn_get_lang_var('prepare_catalog'), false);
	    $reader = new XMLReader();
	    $reader->xml($data, 'utf-8', LIBXML_NOERROR);
        unset($data);

        $catalog = array();
        $catalog_ean = array();
        $catalog_man = array();

        //fn_set_progress('echo', fn_get_lang_var('restrict_catalog'), false);

	    while (@$reader->read()) {
		    if ($reader->nodeType == XMLREADER::ELEMENT) {
                switch ($reader->localName) {
			        case ("file"):
                        $p_id = $reader->getAttribute("Product_ID");
                        $m_id = $reader->getAttribute("Prod_ID");
                        $s_id = $reader->getAttribute("Supplier_id");
                        $catalog[$p_id] = $reader->getAttribute("path");
                        $catalog_man[$s_id][$m_id] = $p_id;
                        break;
                    case ("M_Prod_ID"):
                        if (isset($reader->Supplier_id))
                            $s_id = $reader->getAttribute("Supplier_id");
                        $reader->read();
                        if ($reader->nodeType == XMLReader::TEXT) {
                            $catalog_man[$s_id][$reader->value] = $p_id;
                        }
                        break;
                    case ("EAN_UPC") :
                        $catalog_ean[$reader->getAttribute("Value")] = $p_id;
                        break;
                }
		    }
	    }
	    //fn_print_die($catalog_man["290"]);
        return array ($catalog,$catalog_ean,$catalog_man);
    } else {
        fn_set_notification('E', __('error'), __('error_cant_download_catalog'));
        return false;
    }

}


function get_ICE_suppliers($lang = "EN") {
    // Username and password for usage with ICEcat
    $username = Registry::get('addons.icecat_import.username');
    $password = Registry::get('addons.icecat_import.password');
    $account_type = Registry::get('addons.icecat_import.account_type');

    if ($account_type == "free") {
        $url_request = "http://" . $username . ":" . $password . "@" . "data.Icecat.biz/export/freexml/" . $lang . "/supplier_mapping.xml";
    } else {
        $url_request = "http://" . $username . ":" . $password . "@" . "data.Icecat.biz/export/level4/" . $lang . "/supplier_mapping.xml";
    }

    // Get the product specifications in XML format
    /*$context = stream_context_create(array(
        'http' => array(
            'header'  => "Content-Type: text/xml\r\n" . "Authorization: Basic " . base64_encode($username.":".$password) . "\r\n",
        )
    ));*/

    //fn_set_progress('echo', fn_get_lang_var('request_suppliers'), false);

    $data = file_get_contents($url_request);


    if (!empty($data)) {

        //fn_set_progress('echo', fn_get_lang_var('prepare_suppliers'), false);
        $reader = new XMLReader();
        $reader->xml($data);
        unset($data);

        $suppliers = array();

        //fn_set_progress('echo', fn_get_lang_var('restrict_suppliers'), false);
        while ($reader->read()) {
            if ($reader->nodeType == XMLREADER::ELEMENT) {
                switch ($reader->localName) {
                    case ("SupplierMapping"):
                        $s_id = $reader->getAttribute("supplier_id");
                        $s_name = $reader->getAttribute("name");
                        $suppliers[$s_name] = $s_id;
                        break;
                }
            }
        }
        return array ($suppliers);
    } else {
        fn_set_notification('E', __('error'), __('error_cant_download_catalog'));
        return false;
    }
}

function get_ICE_product_short($type, $value, $vendor = null, $drawdescription = 0, $drawpicture = 0) {
    // Username and password for usage with ICEcat
    $username = Registry::get('addons.icecat_import.username');
    $password = Registry::get('addons.icecat_import.password');

    if ($type == "EAN") {
        if ($type == null && $value == null) return array ("code" => false, "content" => "Unknown error");
    } else if ($type == "MAN") {
        if ($type == null && $value == null && $vendor == null) return array ("code" => false, "content" => "Unknown error");
    }

    // Get the product specifications in XML format
    /*$context = stream_context_create(array(
        'http' => array(
            'header'  => "Authorization: Basic " . base64_encode($username.":".$password)
        )
    ));*/

    if ($type == "EAN") {
        $data = file_get_contents('http://' . $username . ":" . $password . "@" . 'data.icecat.biz/xml_s3/xml_server3.cgi?ean_upc='.$value.';lang=EN;output=productxml', false, $context);
    } else if ($type == "MAN") {
        $data = file_get_contents('http://' . $username . ":" . $password . "@" . 'data.icecat.biz/xml_s3/xml_server3.cgi?prod_id='.$value.';vendor='.$vendor.';lang=EN;output=productxml', false, $context);
    }

    $xml = new SimpleXMLElement($data);
    unset($data);

    if ($xml->Product['Code'] != "1") {
        return array ("code" => false, "content" => (string) $xml->Product['ErrorMessage']);
    }

    return array ("code" => true, "content" => "Empty");

}

function fn_icecat_import_get_product_fields(&$fields) {
    $fields[] = array(
        'name' => '[data][icecat_manufacturer_code]',
        'text' => __('ice_cat_ean_upc')
        );
    $fields[] = array(
        'name' => '[data][icecat_vendor_code]',
        'text' => __('ice_cat_man')
        );
    $fields[] = array(
        'name' => '[data][icecat_product]',
        'text' => __('icecat_product'),
        );
}




?>