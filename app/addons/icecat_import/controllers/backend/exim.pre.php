<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if ( !defined('AREA') ) { die('Access denied'); }

// Set line endings autodetection
ini_set('auto_detect_line_endings', true);
set_time_limit(3600);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	$suffix = '';

	if ($mode == 'icacat_import_products') {
		if (!empty($_REQUEST['product_ids'])) {
            fn_update_ice_products($_REQUEST['import_options'], $_REQUEST['product_ids']);
		} else {
            fn_set_notification('W', __('warning'), __('error_not_selected'));
		}
        return array(CONTROLLER_STATUS_REDIRECT, "exim.icecat_import_page");
	} elseif ($mode == 'icacat_import_all_products') {
        fn_update_ice_products($_REQUEST['import_options']);

        return array(CONTROLLER_STATUS_REDIRECT, "exim.icecat_import_page");
    }


}

if ($mode == 'icecat_import_page') {
	unset($_SESSION['product_ids']);
	unset($_SESSION['selected_fields']);

	$params = $_REQUEST;
	//$params['only_short_fields'] = true;
	$params['icecat_product'] = true;
	//[mviktorov]
	//$params['sort_by'] = 'position';
	//[/mviktorov]

	list($products, $search, $product_count) = fn_get_products($params, 5, DESCR_SL);

	fn_gather_additional_products_data($products, array('get_icon' => false, 'get_detailed' => true, 'get_options' => false, 'get_discounts' => false));

	$view->assign('products', $products);
	$view->assign('search', $search);
} elseif ($mode == 'check_product') {
	if ($_REQUEST['type'] == "EAN") {
		$request = get_ICE_product_short("EAN",$_REQUEST['value']);
	} else if ($_REQUEST['type'] == "MAN") {
		$request = get_ICE_product_short("MAN",$_REQUEST['value'],$_REQUEST['vendor']);
	}

	if ($request['code']) {
		fn_set_notification('N', __('notice'), __('available'));
		fn_icecat_product_avail($_REQUEST['product_id'], $_REQUEST['type'], "Y");
		echo '<div id="check_' . $_REQUEST['type'] . '_' . $_REQUEST['product_id'] . '">'.
                '<div style="color: green;">'. 
                    $_REQUEST['type'] . ' available'.
                 '</div>'.
             '<!--check_' . $_REQUEST['type'] . '_' . $_REQUEST['product_id'] . '-->'.
             '</div>';
	} else {
		fn_set_notification('E', __('error'), $request['content']);
		fn_icecat_product_avail($_REQUEST['product_id'], $_REQUEST['type'], "N");
		echo '<div id="check_' . $_REQUEST['type'] . '_' . $_REQUEST['product_id'] . '">'.
                 '<div style="color: red;">' . 
                     $_REQUEST['type'] . ' not available'.
                 '</div>
                 <!--check_' . $_REQUEST['type'] . '_' . $_REQUEST['product_id'] . '-->'.
             '</div>';

	}

	exit;
}
